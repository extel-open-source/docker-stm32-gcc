FROM ubuntu:18.04

ARG ST_GCC_VER
ENV ST_GCC_VER=$ST_GCC_VER

COPY ./${ST_GCC_VER}-linux.tar.bz2 /source/

RUN apt update -q
RUN apt upgrade -y -q
RUN apt install -y -q wget srecord make git python3-crcmod
RUN tar -xf "/source/${ST_GCC_VER}-linux.tar.bz2"
RUN mv ${ST_GCC_VER} st-gnu-arm-gcc
RUN rm -r /source/

